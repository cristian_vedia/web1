"""Web1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView
from app.principal.views import registerPage, login, update, create, read, delete, index, index1, read_user, delete_user, update_user, logout

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('about/', login_required(TemplateView.as_view(template_name="secret.html"))),
    path('', index.as_view(), name="index"),
    path('index/', index1.as_view(), name="index1"),
    path('register/', registerPage, name="register"),
    path('login/', login, name="login" ),
    path('actualizar/<int:id>', update),
    path('actualizar_user/<int:id>', update_user),
    path('perfil/', create.as_view(),name="perfil"),
    path('read/', read, name="read"),
    path('read_user/', read_user),
    path('delete/<int:id>', delete),
    path('delete_user/<int:id>', delete_user, name="delete_user"),
    path('logout/', logout, name="logout"),
    
]
