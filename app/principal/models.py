from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Perfil(models.Model):
    Ci = models.CharField(max_length=16, null=False, unique=True,error_messages={'required': 'Your C.I. is Required'})
    Nombre = models.CharField(max_length=128, null=False,  error_messages={'required': 'Your Name is Required'}  )
    Apellido = models.CharField(max_length=128, null=False,  error_messages={'required': 'Your Apellido is Required'})
    Correo = models.CharField(max_length=256, null=False,error_messages={'required': 'Your Email is Required'})
    Celular = models.CharField(max_length=16, null=True)
    Whatsapp = models.CharField(max_length=16, null=True)
    Imagen = models.ImageField('Label', upload_to='static/profiles', null=True, blank=True )
    Usuario = models.OneToOneField(User, on_delete=models.CASCADE, null= True, blank=True)

    class meta:

        db_table ="perfil"

