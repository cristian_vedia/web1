
from django.shortcuts import render, HttpResponse, redirect
from django.forms import inlineformset_factory

from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from .forms import CreateUserForm, Perfilform
from django.contrib import messages
from .models import Perfil
from .models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login as do_login, logout
from django.contrib.auth.decorators import login_required

from django.views.generic import TemplateView, UpdateView, CreateView, ListView, RedirectView
from django.views.generic.edit import FormView, CreateView, DeleteView

from django.utils.decorators import method_decorator
from django.urls import reverse_lazy, reverse

# Create your views here.

def registerPage(request):
    form = CreateUserForm()
    if request.method =="POST":
        form = CreateUserForm (request.POST)
        if form.is_valid():
            form.save()
        else:
            print(form)    
            return redirect('index')
    else:
        messages.info(request, 'nombre, email o password incorecto')        
    return render (request, "principal/register.html", {'form':form})

@method_decorator(login_required, name='dispatch')
class index(TemplateView):
   template_name = 'principal/index.html'
class index1(TemplateView):
   template_name = 'principal/index1.html'

    
class create(CreateView):
     model = Perfil
     form_class = Perfilform
     template_name = 'principal/perfil.html'
     success_url = reverse_lazy('index')
     def form_valid(self, form):
        form.instance.created_by_user = self.request.user
        return super(create, self).form_valid(form) 
     def form_invalid(self, form):
        print(form)

#def create(request):
 #   form = Perfilform()
  #  if request.method =="POST":
   #     form = Perfilform (request.POST)
    #
     #   if form.is_valid():
      #      form.save()
       #     return HttpResponseRedirect('../login')    
     #       print(form)
    #return render(request,"principal/perfil.html", {"form":form})

def login(request):
    form =AuthenticationForm() 
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']   
            password = form.cleaned_data['password']
            user =authenticate( username=username, password=password)   
            if user is not None:
                do_login(request, user) 
                return redirect('index')
            else:
                messages.info(request, 'Nombre de Usuario o password incorrecto ')
        else:
              messages.info(request, 'invalido nombre de ususario o contraseña  ')            

    return render(request, "principal/login.html", {'form':form})

def logout(request):
    logout(request)
    return redirect('login')

#class read(ListView):
 #   model = Perfilform
  #  template_name = 'principal/read.html'

def read(request):
       perfil = Perfil.objects.all()
       return render(request, "principal/read.html",{'perfil':perfil})

def read_user(request):
    user = User.objects.all()
    print(user)
    return render(request, "principal/read_user.html",{'user':user})    

def update(request, id):
    perfil = Perfil.objects.get(id=id)
    form = Perfilform(request.POST, instance=perfil)
    if form.is_valid():
        form.save()
        return redirect("/read")
    else: 
        messages.info(request, 'error de datos')    
    return render(request,"principal/update.html", {'perfil':perfil})

def update_user(request, id):    
    user = User.objects.get(id=id)
    form = CreateUserForm(request.POST, instance=user)
    if form.is_valid():
        form.save()
        return redirect("/read_user")
    else:
        messages.info(request, 'Error de  DAtos')    
    return render(request, "principal/update_user.html", {'form':form})
    
def delete(request, id):
    perfil = Perfil.objects.get(id=id)
    perfil.delete()
    return redirect("/read")

def delete_user(request, id):
    user = User.objects.get(id=id)
    user.delete()
    return redirect("/read_user")