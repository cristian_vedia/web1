from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .models import Perfil


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username','email','password1','password2'] 

class Perfilform(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['Ci','Nombre','Apellido','Correo','Celular','Whatsapp','Imagen']

